#ifndef MODULES_ACUMODULE_H_
#define MODULES_ACUMODULE_H_

#include "../Settings.h"
#include "../LightModules/LedStrip.h"
#include "../TimeModules/NetworkTimeServerModule.h"
#include <list>

class AcuModule {
public:
	AcuModule();
	void setup();
	void loop();
	void SetManual(int lightID);
	void sendSearchMove(int number);
	void searchMoveTime(int detectedMove);
	void sendTurnOnLight(int lightID);
	void sendTurnOffLight(int lightID);

private:
	void Everything();

	void sendIntensityLight(int lightID, String step);
	void sendSetIntensityLight(int lightID, String step);
	void sendColorLight(int lightID, String color);
	void sendSetFlowLight(int lightID, int flow);
	void sendSearchGesture();
	WiFiServer server = WiFiServer(80);
	NetworkTimeServerModule ntp;
	NetworkTimeServerModule::MyTime currentTime;
	void printTime();
	LedDiodeRGB ledDiodeRGBGesture = LedDiodeRGB(D1, D2, D3);


};

#endif /* MODULES_ACUMODULE_H_ */
