#include "WiFiModule.h"
WiFiModule::WiFiModule() {
}

WiFiModule::WiFiModule(const char* ssid, const char* password) {
}
WiFiModule::WiFiModule(const char* ssid, const char* password,  IPAddress ip,  IPAddress subnet, const IPAddress gateway) {
}

bool WiFiModule::Connect(const char* ssid, const char* password) {
	Serial.begin(115200);

	if (ssid && !ssid[0]) {
		printf("Network name can't be empty\n");
		return false;
	}

	WiFi.begin(ssid, password);

	wl_status_t status = WiFi.status();
	while (status != WL_CONNECTED && status != WL_NO_SSID_AVAIL
			&& status != WL_CONNECT_FAILED) {
		delay(1000);
		Serial.print(".");
		status = WiFi.status();
	}
	if (status == WL_NO_SSID_AVAIL) {
		Serial.println("");
		Serial.println("Network with this name does not exist.");
		return false;
	} else {
	Serial.println("");
	Serial.println("WiFi connected");

	// Print the IP address
	Serial.println(WiFi.localIP());
	}
	return true;
}
bool WiFiModule::Connect(const char* ssid, const char* password, IPAddress ip,
		IPAddress subnet, IPAddress gateway) {
	Serial.begin(115200);

	if (ssid && !ssid[0]) {
		Serial.println("Network name can't be empty");
		return false;
	}

	WiFi.config(ip, gateway, subnet);
	WiFi.begin(ssid, password);

	wl_status_t status = WiFi.status();
	while (status != WL_CONNECTED && status != WL_CONNECT_FAILED
			&& status != WL_NO_SSID_AVAIL) {
		delay(1000);
		Serial.print(".");
		status = WiFi.status();
	}
	if (status == WL_CONNECT_FAILED) {
		Serial.println("WiFi connection failed.");
		return false;
	} else if (status == WL_NO_SSID_AVAIL) {
		Serial.println("");
		Serial.println("Network with this name does not exist.");
		return false;
	} else if (status == WL_CONNECTED) {
		Serial.println("");
		Serial.println("WiFi connected.");

		// Print the IP address
		Serial.println(WiFi.localIP());

		return true;
	}
	Serial.println("WiFi unexpected error.");
	return false;
}

