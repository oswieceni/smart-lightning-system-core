#include "KobiIr.h"
#define DEBUG

KobiIr::KobiIr(byte pinIr, int brightness, KobiIr::State state,
		String color) {
	this->pinIr = pinIr;
	pinMode(pinIr, OUTPUT);
	irTurnOff();
	if (brightness < 0) {
		turnOn();
		sendCommand(COMMAND_White);
		sendCommand(COMMAND_Up);
	} else {
		this->currentBrightness = brightness;
		this->currentState = state;
		this->color = color;
	}
}

KobiIr::State KobiIr::getState() {
	return currentState;
}

int KobiIr::getBrightness() {
	return currentBrightness;
}


String KobiIr::getColor() {
	return color;
}

void KobiIr::irTurnOff() {
	digitalWrite(pinIr, LOW);
	digitalWrite(pinLed, LOW);
}

void KobiIr::irTurnOff(unsigned int time) {
	irTurnOff();
	delayMicroseconds(time);
}

void KobiIr::changeColor(String color) {
	if (color == "ee1010") {
		sendCommand(COMMAND_Red);
	} else if (color == "f06e3a") {
		sendCommand(COMMAND_RedOrange);
	} else if (color == "d27459") {
		sendCommand(COMMAND_Orange);
	} else if (color == "f6a443") {
		sendCommand(COMMAND_OrangeYellow);
	} else if (color == "ebd46a") {
		sendCommand(COMMAND_Yellow);
	} else if (color == "37c759") {
		sendCommand(COMMAND_Green);
	} else if (color == "7bf198") {
		sendCommand(COMMAND_GreenSpring);
	} else if (color == "83b08e") {
		sendCommand(COMMAND_GreenCyan);
	} else if (color == "61e1c8") {
		sendCommand(COMMAND_Cyan);
	} else if (color == "27a6d8") {
		sendCommand(COMMAND_CyanBlue);
	} else if (color == "044bb5") {
		sendCommand(COMMAND_Blue);
	} else if (color == "5373ce") {
		sendCommand(COMMAND_BlueViolet);
	} else if (color == "813991") {
		sendCommand(COMMAND_Violet);
	} else if (color == "e13b77") {
		sendCommand(COMMAND_VioletPink);
	} else if (color == "e557ab") {
		sendCommand(COMMAND_Pink);
	} else if (color == "ffffff") {
		sendCommand(COMMAND_White);
	}
	this->color = color;
}


void KobiIr::animate(int speed) {
	switch (speed) {
	case 0:
		sendCommand(COMMAND_Smooth);
		break;
	case 1:
		sendCommand(COMMAND_Fade);
		break;
	case 2:
		sendCommand(COMMAND_Strobe);
		break;
	case 3:
		sendCommand(COMMAND_Flash);
		break;
	}
}

void KobiIr::brighten(){
	if (currentBrightness < 9) {
		sendCommand(COMMAND_Up);
		resendCommand();
		resendCommand();
		resendCommand();
		currentBrightness++;

#if defined(DEBUG)
		Serial.printf("Jasniej %d \n", currentBrightness);
#endif
	}
}

void KobiIr::darken(){
	if (currentBrightness >= 1) {
		sendCommand(COMMAND_Down);
		resendCommand();
		resendCommand();
		resendCommand();
		currentBrightness--;

#if defined(DEBUG)
		Serial.printf("Przyciemnienie %d \n", currentBrightness);
#endif
	}
}

void KobiIr::setBrightness(int brightness){
	int currBrightness = currentBrightness;
	brightness--;

	if (brightness / 10 >= currBrightness) {
		for (int i = 0; i < (brightness/10 - currBrightness); i++){
				brighten();
				delay(400);
		}
	}
	else if (brightness/10 < currBrightness ){
		for (int i = 0; i < (currBrightness - brightness/10); i++){
				darken();
				delay(400);
		}
	}

}

void KobiIr::turnOn() {
	if (currentState != on) {
		sendCommand(COMMAND_On);
		currentState = on;
	}
}

void KobiIr::turnOff() {
	if (currentState != off) {
		sendCommand(COMMAND_Off);
		currentState = off;
	}
}

void KobiIr::sendCommand(unsigned char data) {
	bitInitialization();
	sendByte(ADDRESS_1);
	sendByte(ADDRESS_2);

	sendByte(data);
	sendByte(data ^ 0xff);

	bitLow(); // stop-bit
}

void KobiIr::resendCommand() {
	irTurnOn(9000);
	irTurnOff(2250);
	bitLow(); // stop-bit
}

void KobiIr::bitInitialization() {
	irTurnOn(9000);
	irTurnOff(4500);
}

void KobiIr::bitLow() {
	// 0.55ms
	irTurnOn(560);
	irTurnOff(560);
}
void KobiIr::bitHigh() {
	// 1.65ms
	irTurnOn(560);
	irTurnOff(1650);
}

void KobiIr::sendByte(unsigned char data) {
	unsigned char i;

	for (i = 0; i < 8; i++)
			{
		if ((data << i) & 0x80) {
			bitHigh();
		} else {
			bitLow();
		}
	}
}

void KobiIr::irTurnOn(unsigned int time) {
	unsigned long beginning = micros();
	unsigned long current = beginning;
	digitalWrite(pinLed, HIGH);
	while (current - beginning < time) {
		digitalWrite(pinIr, HIGH);
		delayMicroseconds(halfPeriodicTime);
		digitalWrite(pinIr, LOW);
		delayMicroseconds(halfPeriodicTime);
		current = micros();
		if (current < beginning)
			beginning = current;
	}
	digitalWrite(pinLed, LOW);
}

