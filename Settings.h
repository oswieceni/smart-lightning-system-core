#ifndef SETTINGS_H_
#define SETTINGS_H_
#include <IPAddress.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include "WiFiModules/WiFiModule.h"
#include "ThirdPartyLibraries/aREST/aREST.h"
#include "LightModules/LedDiodeRGB.h"

class Settings {
public:
	static IPAddress getAcuIp();
	static IPAddress getIntensityModuleIp();
	static String getSsid();
	static String getPassword();
	static IPAddress getSubnet();
	static IPAddress getApiIP();
	static IPAddress getGateway();
	static IPAddress getLightModuleIP(int lightModuleId);
	static IPAddress getGestureModuleIP();
	static IPAddress getMotionModuleIP();
private:
	Settings() {
	}
};

#endif /* SETTINGS_H_ */
