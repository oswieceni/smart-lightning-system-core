#include "SensorAPDS9960.h"

volatile bool gestureFlag = 0;

void gestureInterrupt() {
	gestureFlag = 1;
}


SensorAPDS9960::SensorAPDS9960() {
}

void SensorAPDS9960::init() {
	pinMode(APDS9960_INT, INPUT);
	// Initialize interrupt service routine
	attachInterrupt(APDS9960_INT, gestureInterrupt, FALLING);

	// Initialize APDS-9960 (configure I2C and initial values)
	if (apds9960.init()) {
		Serial.println("APDS-9960 initialization complete");
	} else {
		Serial.println("Something went wrong during APDS-9960 init!");
	}

	// Start running the APDS-9960 gesture sensor engine
	if (apds9960.enableGestureSensor(true)) {
		Serial.println("Gesture sensor is now running");
	} else {
		Serial.println("Something went wrong during gesture sensor init!");
	}
}

SensorAPDS9960::Gesture SensorAPDS9960::handleGesture() {
	SensorAPDS9960::Gesture gesture = NONE;
	if (gestureFlag == 1) {
		detachInterrupt(APDS9960_INT);
		if (SensorAPDS9960::apds9960.isGestureAvailable()) {
			Serial.println("SHOW SOMETHING!");
			switch (SensorAPDS9960::apds9960.readGesture()) {
			case DIR_UP:
				Serial.println("UP");
				gesture = UP;
				break;
			case DIR_DOWN:
				Serial.println("DOWN");
				gesture = DOWN;
				break;
			case DIR_LEFT:
				Serial.println("LEFT");
				gesture = LEFT;
				break;
			case DIR_RIGHT:
				Serial.println("RIGHT");
				gesture = RIGHT;
				break;
			case DIR_NEAR:
				Serial.println("NEAR");
				gesture = NEAR;
				break;
			case DIR_FAR:
				Serial.println("FAR");
				gesture = FAR;
				break;
			default:
				Serial.println("NONE");
				gesture = NONE;
			}
		}
		gestureFlag = 0;
		attachInterrupt(APDS9960_INT, gestureInterrupt, FALLING);
	}
	return gesture;
}
