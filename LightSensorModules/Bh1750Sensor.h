#ifndef LIGHTSENSORMODULES_BH1750SENSOR_H_
#define LIGHTSENSORMODULES_BH1750SENSOR_H_
#include "../BH1750/BH1750.h"

class Bh1750Sensor {
private:
	BH1750 Bh1750;

public:
	Bh1750Sensor();
	uint16_t readLightLevel();
};

#endif /* LIGHTSENSORMODULES_BH1750SENSOR_H_ */
