/*
 * Button.h
 *
 *  Created on: Apr 11, 2017
 *      Author: maciej
 */

#ifndef KEYBOARDMODULES_BUTTON_H_
#define KEYBOARDMODULES_BUTTON_H_
#include <Arduino.h>

class Button {
public:
	/**
	 * \param buttonPin Number of Pin to which button is connected
	 */
	Button(byte buttonPin);

	/**
	 * \brief Check if button is currently pressed
	 * \return true if button is pressed otherwise false
	 */
	bool IsPressed();

private:
	byte buttonPin;
};

#endif /* KEYBOARDMODULES_BUTTON_H_ */
