#ifndef LIGHTMODULES_LEDDIODERGB_H_
#define LIGHTMODULES_LEDDIODERGB_H_
#include "ESP8266WiFi.h"
#include <pwm.h>
class LedDiodeRGB {
private:
	byte portR;
	byte portG;
	byte portB;
public:
	LedDiodeRGB(byte portR, byte portG, byte portB);
	void SetBrightness(byte brightness);
	void setColorRGB(int rR, int gG, int bB);
};

#endif /* LIGHTMODULES_LEDDIODERGB_H_ */
