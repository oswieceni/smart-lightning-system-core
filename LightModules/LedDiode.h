#ifndef LIGHTMODULES_LEDDIODE_H_
#define LIGHTMODULES_LEDDIODE_H_

#include <Arduino.h>

class LedDiode {
private:
	byte port;
public:
	LedDiode(byte port);
	void SetBrightness(byte brightness);
};

#endif /* LIGHTMODULES_LEDDIODE_H_ */
