#include "Settings.h"

IPAddress Settings::getAcuIp() {
	IPAddress ip;
	ip.fromString("192.168.99.130");
	return ip;
}

IPAddress Settings::getIntensityModuleIp() {
	IPAddress ip;
	ip.fromString("192.168.99.136");
	return ip;
}

IPAddress Settings::getLightModuleIP(int lightModuleId) {
	IPAddress ip;
	if (lightModuleId == 777780) { // KobiIr
		ip.fromString("192.168.99.132");
	} else if (lightModuleId == 777779) {
		ip.fromString("192.168.99.133"); // LedStrip
	}
	else if (lightModuleId == 777776) {
		ip.fromString("192.168.99.134"); // KobiIr
	}
	return ip;
}

IPAddress Settings::getGestureModuleIP() {
	IPAddress ip;
	ip.fromString("192.168.99.131");
	return ip;
}

IPAddress Settings::getMotionModuleIP() {
	IPAddress ip;
	ip.fromString("192.168.99.129");
	return ip;
}

String Settings::getSsid() {
	return "SLS";
}

String Settings::getPassword() {
	return "oswieceni";
}

IPAddress Settings::getSubnet() {
	IPAddress ip;
	ip.fromString("255.255.255.0");
	return ip;
}

IPAddress Settings::getGateway() {
	IPAddress ip;
	ip.fromString("192.168.99.1");
	return ip;
}

IPAddress Settings::getApiIP() {
	IPAddress ip;
	ip.fromString("192.168.99.87");
	return ip;
}
