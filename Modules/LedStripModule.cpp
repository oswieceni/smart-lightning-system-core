#include "LedStripModule.h"
int currentBrightness = 9;
LedStrip::State currentState;
String currentColor;

int setOnLed(String command) {
	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);
	ledStrip.turnOn();
	currentState = ledStrip.getState();
	Serial.println("Wlaczam się 7777779");

	return 1;
}

int setOffLed(String command) {
	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);
	ledStrip.turnOff();
	currentState = ledStrip.getState();
	Serial.println("Wylaczam się 777779");

	return 1;
}

int setIntensityLed(String command) {
	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);
	int step = command.toInt();
	if (step == 1) {
		ledStrip.brighten();
	} else
		ledStrip.darken();
	currentBrightness = ledStrip.getBrightness();
	Serial.println("Ustawiam natezenie 777779");

	return 1;
}

int setLevelIntensityLed(String command) {

	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);
	int level = command.toInt();
	ledStrip.setBrightness(level);
	currentBrightness = ledStrip.getBrightness();
	Serial.println("Ustawiam natezenie 777779");

	return 1;
}

int setColorLed(String command) {
	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);

	ledStrip.changeColor(command);
	currentBrightness = ledStrip.getBrightness();
	currentColor = ledStrip.getColor();
	Serial.println("Ustawiam kolor 777779");

	return 1;
}

int setAnimateLed(String command) {
	LedStrip ledStrip = LedStrip(irPin, currentBrightness, currentState,
			currentColor);
	int speed = command.toInt();
	ledStrip.animate(speed);
	Serial.println("Ustawiam flow 777779");
	return 1;
}

LedStripModule::LedStripModule() {

}

void LedStripModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(), Settings::getLightModuleIP(id),
			Settings::getSubnet(), Settings::getGateway())) {
		ledDiodeRGBGesture.setColorRGB(200, 0, 0);
		return;
	}
	rest.function((char*) "Lon", setOnLed);
	rest.function((char*) "Lof", setOffLed);
	rest.function((char*) "LCo", setColorLed);
	rest.function((char*) "Lin", setIntensityLed);
	rest.function((char*) "LPI", setLevelIntensityLed);
	rest.function((char*) "Lfl", setAnimateLed);
	Serial.println("Jestem odpowiedzialny za pasek led");
	WiFiClient client;
	rest.set_id((char*) id);
	rest.set_name((char*) "LedStrip");
	server.begin();
	Serial.println("Server started");
	LedStrip ledStrip = LedStrip(irPin, -1, LedStrip::unknown, currentColor);
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);
}

void LedStripModule::loop() {
	WiFiClient client = server.available();
	if (!client) {
	} else {
		while (!client.available()) {
			delay(1);

		}
		rest.handle(client);
	}
}
