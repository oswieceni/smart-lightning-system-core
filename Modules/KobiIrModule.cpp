#include "KobiIrModule.h"

int currentBrightnessBulb = 9;
LedStrip::State currentStateBulb;
String currentColorBulb;

int setOnBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin,
			currentBrightnessBulb,
			currentStateBulb, currentColorBulb);

	lightBulb.turnOn();
	currentStateBulb = lightBulb.getState();
	Serial.println("Wlaczam się 7777776");
	return 1;
}

int setOffBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin,
			currentBrightnessBulb,
			currentStateBulb, currentColorBulb);

	lightBulb.turnOff();
	currentStateBulb = lightBulb.getState();
	Serial.println("Wylaczam się 777776");
	return 1;
}

int setIntensityBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin,
			currentBrightnessBulb,
			currentStateBulb, currentColorBulb);

	int step = command.toInt();
	Serial.printf("Zarowka odebrala: %d\n", step);
	if (step == 1) {

		lightBulb.brighten();
	} else
		lightBulb.darken();
	currentBrightnessBulb = lightBulb.getBrightness();
	Serial.println("Ustawiam natezenie 777776");
	return 1;
}

int setLevelIntensityBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin,
			currentBrightnessBulb,
			currentStateBulb, currentColorBulb);

	int level = command.toInt();
	lightBulb.setBrightness(level);
	currentBrightnessBulb = lightBulb.getBrightness();
	Serial.println("Ustawiam natezenie 777776");

	return 1;
}

int setColorBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin,
			currentBrightnessBulb,
			currentStateBulb, currentColorBulb);

	lightBulb.changeColor(command);
	currentBrightnessBulb = lightBulb.getBrightness();
	currentColorBulb = lightBulb.getColor();
	Serial.println("Ustawiam kolor 777776");

	return 1;
}

int setAnimateBulb(String command) {
	LedStrip lightBulb = LedStrip(irPin, currentBrightnessBulb,
			currentStateBulb, currentColorBulb);
	int speed = command.toInt();
	lightBulb.animate(speed);
	Serial.println("Ustawiam flow 777776");

	return 1;
}

KobiIrModule::KobiIrModule() {

}

void KobiIrModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(),
			Settings::getLightModuleIP(id.toInt()),
			Settings::getSubnet(), Settings::getGateway()))
	{
		return;
	}

	rest.function((char*) "Lon", setOnBulb);
	rest.function((char*) "Lof", setOffBulb);
	rest.function((char*) "LCo", setColorBulb);
	rest.function((char*) "Lin", setIntensityBulb);
	rest.function((char*) "LPI", setLevelIntensityBulb);
	rest.function((char*) "Lfl", setAnimateBulb);
	Serial.println("Jestem odpowiedzialny za zarowke");
	WiFiClient client;
	rest.set_id((char*) id.c_str());
	rest.set_name((char*) "LightBulb");
	server.begin();
	Serial.println("Server started");
	LedStrip lightBulb = LedStrip(irPin, -1,
			LedStrip::unknown,
			currentColorBulb);
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);
}

void KobiIrModule::loop() {
	WiFiClient client = server.available();
	if (!client) {
	} else {
		while (!client.available()) {
			delay(1);
		}
		rest.handle(client);
	}
}
