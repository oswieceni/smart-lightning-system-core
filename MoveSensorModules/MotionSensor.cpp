#include "MotionSensor.h"
#include <Arduino.h>

MotionSensor::MotionSensor() {

}

/*MotionSensor::MotionSensor(byte motionSensorPin) {
	this->motionSensorPin = motionSensorPin;
	pinMode(this->motionSensorPin, INPUT);
 }*/

void MotionSensor::configure(byte motionSensorPin) {
	this->motionSensorPin = motionSensorPin;
	pinMode(this->motionSensorPin, INPUT);
}

bool MotionSensor::MotionDetected() {
	delay(500);
	return digitalRead(motionSensorPin) == HIGH;
}
