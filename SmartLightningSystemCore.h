// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _SmartLightningSystemCore_H_
#define _SmartLightningSystemCore_H_

#include "ESP8266WiFi.h"
#include "Modules/AcuModule.h"
#include "Modules/IntensityModule.h"
#include "Modules/GestureModule.h"
#include "Modules/KobiIrModule.h"
#include "Modules/LedStripModule.h"
#include "Modules/MotionModule.h"

#define UINT_MAX 4294967295

#endif /* _SmartLightningSystemCore_H_ */
