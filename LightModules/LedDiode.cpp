#include "LedDiode.h"
#include "Arduino.h"

LedDiode::LedDiode(byte port) {
	this->port = port;
	digitalWrite(this->port, OUTPUT);
}

void LedDiode::SetBrightness(byte brightness) {
	analogWrite(port, brightness);
}
