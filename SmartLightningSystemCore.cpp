#include "./SmartLightningSystemCore.h"
#include "Settings.h"

//#define acu
//#define intensity
#define kobi
//#define led
//#define gesture
//#define motion


#if defined (acu)
AcuModule module = AcuModule();
#elif defined (intensity)
IntensityModule module = IntensityModule();
#elif defined (kobi)
KobiIrModule module = KobiIrModule();
#elif defined (led)
LedStripModule module = LedStripModule();
#elif defined (gesture)
GestureModule module = GestureModule();
#elif defined (motion)
MotionModule module = MotionModule();
#endif

void setup() {
	Serial.begin(115200);
	Serial.println();
	module.setup();
}

void loop() {
	module.loop();
}
