#include "Arduino.h"
#ifndef LIGHTMODULES_LEDSTRIP_H_
#define LIGHTMODULES_LEDSTRIP_H_

#define COMMAND_On            0xE0
#define COMMAND_Off           0x60

#define COMMAND_Down          0x20
#define COMMAND_Up            0xA0

#define COMMAND_Flash         0xF0
#define COMMAND_Strobe        0xE8
#define COMMAND_Fade          0xD8
#define COMMAND_Smooth        0xC8

#define COMMAND_White         0xD0

#define COMMAND_Red           0x90
#define COMMAND_RedOrange     0xB0
#define COMMAND_Orange        0xA8
#define COMMAND_OrangeYellow  0x98
#define COMMAND_Yellow        0x88

#define COMMAND_Green         0x10
#define COMMAND_GreenSpring   0x30
#define COMMAND_GreenCyan     0x28
#define COMMAND_Cyan          0x18
#define COMMAND_CyanBlue      0x08

#define COMMAND_Blue          0x50
#define COMMAND_BlueViolet    0x70
#define COMMAND_Violet        0x68
#define COMMAND_VioletPink    0x58
#define COMMAND_Pink          0x48

#define ADDRESS_1    0x00
#define ADDRESS_2    0xFF

#define halfPeriodicTime  12    // 500/38;

class LedStrip {
public:
	enum State {
		unknown, off, on
	};


	LedStrip(byte pinIr, int brightness, LedStrip::State state,
			String color);
	void changeColor(String color);
	void animate(int speed);
	void turnOn();
	void turnOff();
	String getColor();
	void brighten();
	void darken();
	void setBrightness(int brightness);
	int getBrightness();
	State getState();

private:
	byte pinIr;
	byte pinLed;
	String color;
	int currentBrightness = 9;
	void irTurnOff();
	void irTurnOff(unsigned int time);
	void bitInitialization();
	void bitLow();
	void bitHigh();
	void sendByte(unsigned char data);
	void irTurnOn(unsigned int time);
	void sendCommand(unsigned char data);
	void resendCommand();
	State currentState = unknown;
};

#endif /* LIGHTMODULES_LEDSTRIP_H_ */
