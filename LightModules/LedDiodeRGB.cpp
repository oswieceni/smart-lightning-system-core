#include "LedDiodeRGB.h"
//#define ANODA

LedDiodeRGB::LedDiodeRGB(byte portR, byte portG, byte portB) {
	this->portR = portR;
	this->portG = portG;
	this->portB = portB;
	pinMode(this->portR, OUTPUT);
	pinMode(this->portG, OUTPUT);
	pinMode(this->portB, OUTPUT);
}

void LedDiodeRGB::setColorRGB(int rR, int gG, int bB) {
#if defined(ANODA)
	analogWrite(portR, 1024 - rR * 4);
	analogWrite(portG, 1024 - gG * 4);
	analogWrite(portB, 1024 - bB * 4);
#else
	analogWrite(portR, rR * 4);
	analogWrite(portG, gG * 4);
	analogWrite(portB, bB * 4);
#endif
}
