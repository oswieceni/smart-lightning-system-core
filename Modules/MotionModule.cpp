#include "MotionModule.h"
#define DEBUG
bool searchMove = false;


int moveSearch(String command) {
	if (command.toInt() == 1) {
		searchMove = true;
		Serial.println("Jedziemy czujnik ruchu!!!");
	} else
		searchMove = false;
	return 1;
}

void MotionModule::sendMove(int detected) {
	WiFiClient client;
	delay(10);
	if (!client.connect(Settings::getAcuIp(), 80)) {
		Serial.println("Brak polaczenia.");
		return;
	}
	client.println("GET /moT?params=" + String(detected) + " HTTP/1.1"); // GET http://192.168.0.130/moV
	String host = "Host: " + Settings::getAcuIp().toString() + ":80";
	client.println(host);
	client.println("Connection: close");
	client.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	client.stop();
}


MotionModule::MotionModule() {
	motionSensor.configure(D4);
}

void MotionModule::motionModuleSearch() {

	if (motionSensor.MotionDetected()) {
		detectedMove = 1;
		if (detectedMove != detectedMoveOld) {
#if defined(DEBUG)
		Serial.println("Jest ruch");
#endif
		}
	} else {
		detectedMove = 0;
		if (detectedMove != detectedMoveOld) {
#if defined(DEBUG)
			Serial.println("Brak ruchu");
#endif
		}
	}
	if (detectedMove != detectedMoveOld) {
		detectedMoveOld = detectedMove;
		sendMove(detectedMove);
	}
}

void MotionModule::loop() {
	WiFiClient client = server.available();
	if (!client) {
		if (searchMove == true) {
			motionModuleSearch();
		}
	} else {
		while (!client.available()) {
			delay(1);
		}
		rest.handle(client);
	}
}

void MotionModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(), Settings::getMotionModuleIP(),
			Settings::getSubnet(), Settings::getGateway()))
	{
		ledDiodeRGBGesture.setColorRGB(200, 0, 0);
		return;
	}
	rest.function((char*) "moC", moveSearch);
	Serial.println("Jestem czujnikiem ruchu!");
	WiFiClient client;
	rest.set_id((char*) "777782");
	rest.set_name((char*) "MoveModule");
	server.begin();
	Serial.println("Server started");
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);

}
