#ifndef MOVESENSORMODULES_MOTIONSENSOR_H_
#define MOVESENSORMODULES_MOTIONSENSOR_H_
#include <Arduino.h>

#define motionSensorPin D4


class MotionSensor {
public:
	/**
	 * \param motionSensorPin Number of Pin to which sensor is connected
	 */
//	MotionSensor(byte motionSensorPin);

	MotionSensor();

	void configure(byte pin);

	/**
	 * \ Check if there is any movement
	 * \return true if motion was detected, otherwise false
	 */
	bool MotionDetected();

private:
	byte motionSensorPin;
};

#endif /* MOVESENSORMODULES_MOTIONSENSOR_H_ */

