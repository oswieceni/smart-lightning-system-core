#ifndef MODULES_LEDSTRIPMODULE_H_
#define MODULES_LEDSTRIPMODULE_H_

#define irPin D3    // pin do którego jest podłączona dioda podczerwona

#include <WiFiUdp.h>
#include <WiFiClient.h>
#include "ESP8266WiFi.h"
#include "../WiFiModules/WiFiModule.h"
#include "../LightModules/LedStrip.h"
#include "../ThirdPartyLibraries/aREST/aREST.h"
#include "../Settings.h"


class LedStripModule {
public:
	LedStripModule();
	void loop();
	void setup();
	int id = 777779;

private:
	aREST rest = aREST();
	WiFiServer server = WiFiServer(80);
	LedDiodeRGB ledDiodeRGBGesture = LedDiodeRGB(D7, D6, D5);

};

#endif /* MODULES_LEDSTRIPMODULE_H_ */
