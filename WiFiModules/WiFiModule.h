#ifndef WIFIMODULES_WIFIMODULE_H_
#define WIFIMODULES_WIFIMODULE_H_

#include <ESP8266WiFi.h>

class WiFiModule {

public:
	WiFiModule();
	WiFiModule(const char* ssid, const char* password);
	WiFiModule(const char* ssid, const char* password,  IPAddress ip,  IPAddress subnet,  IPAddress gateway );
	bool Connect(const char* ssid,
			const char* password);bool Connect(
			const char* ssid, const char* password, IPAddress ip,
			IPAddress subnet, IPAddress gateway);
};

#endif /* WIFIMODULES_WIFIMODULE_H_ */
