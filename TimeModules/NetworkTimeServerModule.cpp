/*
 * NetworkTimeServerModule.cpp
 *
 *  Created on: Apr 15, 2017
 *      Author: maciej
 */

#include "NetworkTimeServerModule.h"

NetworkTimeServerModule::NetworkTimeServerModule() {
	delay = 0;
	currentTime.hour = 0;
	currentTime.minute = 0;
	currentTime.second = 0;
}

void NetworkTimeServerModule::Start(unsigned udpPort) {
	udp.begin(udpPort);
	WiFi.hostByName(ntpServerName, timeServerIP);
	Serial.printf("Time server: %s\n", timeServerIP.toString().c_str());
}

NetworkTimeServerModule::MyTime NetworkTimeServerModule::GetTime() {
	return currentTime;
}

bool NetworkTimeServerModule::TimeIsChanged() {
	//Przekopiowanie starego czasu
	if (previousTime.second != currentTime.second) {
		previousTime.hour = currentTime.hour;
		previousTime.minute = currentTime.minute;
		previousTime.second = currentTime.second;
	}
	delay = delay + 1;
	if (delay == 1) {
		sendNTPpacket(timeServerIP);
	} else if (delay == 500000UL) {
		int timestamp = udp.parsePacket();
		if (!timestamp) {
			if (wasReplay) {
				delay = 0;
			} else {
				delay = delay - 100000UL;
				wasReplay = true;
			}
		} else {
			udp.read((char*) packetBuffer, NTP_PACKET_SIZE);

			unsigned highWord = word(packetBuffer[40], packetBuffer[41]);
			unsigned lowWord = word(packetBuffer[42], packetBuffer[43]);
			unsigned secsSince1900 = highWord << 16 | lowWord;
			const unsigned seventyYears = 2208988800UL;
			unsigned epoch = secsSince1900 - seventyYears;

			// Obliczenie godziny, minuty i sekundy
			currentTime.hour = GMT + (epoch % 86400L) / 3600;
			currentTime.minute = (epoch % 3600) / 60;
			currentTime.second = epoch % 60;

			if (currentTime.hour >= 24)
				currentTime.hour -= 24;
			delay = 0;

			return true;
		}
		}
	return false;
}


// Wysyłanie do serweru czasu
unsigned NetworkTimeServerModule::sendNTPpacket(IPAddress& address)
{
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011;
  packetBuffer[1] = 0;
  packetBuffer[2] = 6;
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  udp.beginPacket(address, 123);
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
