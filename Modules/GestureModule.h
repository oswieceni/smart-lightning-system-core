#ifndef MODULES_GESTUREMODULE_H_
#define MODULES_GESTUREMODULE_H_

#include <WiFiUdp.h>
#include <WiFiClient.h>
#include "ESP8266WiFi.h"
#include "../WiFiModules/WiFiModule.h"
#include "../GestureSensorModules/SensorAPDS9960.h"
#include "../ThirdPartyLibraries/aREST/aREST.h"
#include "../Settings.h"


class GestureModule {
public:
	GestureModule();
	void setup();
	void loop();
private:
	aREST rest = aREST();
	WiFiServer server = WiFiServer(80);
	SensorAPDS9960 sensorApds = SensorAPDS9960();
	LedDiodeRGB ledDiodeRGBGesture = LedDiodeRGB(D3, D4, D8);
	int requestDelayGesture = 0;

	void readGesture();
};

#endif /* MODULES_GESTUREMODULE_H_ */
