/*
 * Button.cpp
 *
 *  Created on: Apr 11, 2017
 *      Author: maciej
 */

#include "Button.h"
#include <Arduino.h>

Button::Button(byte buttonPin) {
	this->buttonPin = buttonPin;
	pinMode(this->buttonPin, INPUT);
}

bool Button::IsPressed() {
	return digitalRead(buttonPin) == HIGH;
}
