#include "Bh1750Sensor.h"
//#define DEBUG

Bh1750Sensor::Bh1750Sensor() {
	Bh1750.begin();
}

uint16_t Bh1750Sensor::readLightLevel() {
	delay(700);
	uint16_t lux = Bh1750.readLightLevel(); //Pobranie wartości natężenia światła
#if defined(DEBUG)
	Serial.print("Wartosc natezenia swiatla: ");
	Serial.print(lux);    // Natężenie podane w jednostce luks
	Serial.println(" lx");
#endif
	return lux;
}

