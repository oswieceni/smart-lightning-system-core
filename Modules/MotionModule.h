#ifndef MODULES_MOTIONMODULE_H_
#define MODULES_MOTIONMODULE_H_

//#define motionSensorPin D    // pin do którego podłączony jest czujnik ruchu

#include <WiFiUdp.h>
#include <WiFiClient.h>
#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "../WiFiModules/WiFiModule.h"
#include "../MoveSensorModules/MotionSensor.h"
#include "../ThirdPartyLibraries/aREST/aREST.h"
#include "../Settings.h"

class MotionModule {
public:
	MotionModule();
	void setup();
	void loop();
	void motionModuleSearch();
private:
	aREST rest = aREST();
	WiFiServer server = WiFiServer(80);
	MotionSensor motionSensor = MotionSensor();
	void sendMove(int detected);
	int detectedMove = 1;
	bool movechange = false;
	int detectedMoveOld;
	LedDiodeRGB ledDiodeRGBGesture = LedDiodeRGB(D1, D3, D8);

};

#endif /* MODULES_MOTIONMODULE_H_ */
