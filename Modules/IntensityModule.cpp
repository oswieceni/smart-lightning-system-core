#include "IntensityModule.h"
#define DEBUG

bool searchIntensity = false;

int intensitySearch(String command) {
	if (command.toInt() == 1) {
		searchIntensity = true;
		Serial.println("Czujnik natezenia, sprawdzam!");
	} else
		searchIntensity = false;
	return 1;
}

IntensityModule::IntensityModule() {

}

void IntensityModule::readIntensity() {
	int intensity = intensitySensor.readLightLevel();
#if defined(DEBUG)
	Serial.println(intensity);
#endif
	WiFiClient client;
	if (!client.connect(Settings::getAcuIp(), 80)) {
		Serial.println("Brak polaczenia.");
		return;
	}

	client.println("GET /inC?params=" + String(intensity) + " HTTP/1.1"); // GET http://192.168.0.130/inC
	String host = "Host: " + Settings::getAcuIp().toString() + ":80";
	client.println(host);
	client.println("Connection: close");
	client.println();
#if defined(DEBUG)
	Serial.print("Natezenie wysyla do:");
	Serial.println(host);
#endif
	delay(10);
	client.stop();
	searchIntensity = false;
}



void IntensityModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(), Settings::getIntensityModuleIp(),
			Settings::getSubnet(), Settings::getGateway()))
		{
		ledDiodeRGBGesture.setColorRGB(200, 0, 0);
		return;
	}
	rest.function((char*) "inC", intensitySearch);
	Serial.println("Jestem czujnikiem natezenia swiatla!");
	WiFiClient client;
	rest.set_id((char*) "777778");
	rest.set_name((char*) "IntensityModule");
	server.begin();
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);

}

void IntensityModule::loop() {
	WiFiClient client = server.available();
	if (!client) {
		if (searchIntensity) {
			readIntensity();
		}
	} else {
		while (!client.available()) {
			delay(1);
		}
		rest.handle(client);
	}
}
