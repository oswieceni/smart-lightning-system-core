#include "Arduino.h"

#ifndef LIGHTMODULES_KOBIIR_H_
#define LIGHTMODULES_KOBIIR_H_

#define COMMAND_On            0xC0
#define COMMAND_Off           0x40

#define COMMAND_Down          0x80
#define COMMAND_Up            0x00

#define COMMAND_Flash         0xD0
#define COMMAND_Strobe        0xF0
#define COMMAND_Fade          0xC8
#define COMMAND_Smooth        0xE8

#define COMMAND_White         0xE0

#define COMMAND_Red           0x20
#define COMMAND_RedOrange     0x10
#define COMMAND_Orange        0x30
#define COMMAND_OrangeYellow  0x08
#define COMMAND_Yellow        0x28

#define COMMAND_Green         0xA0
#define COMMAND_GreenSpring   0x90
#define COMMAND_GreenCyan     0xB0
#define COMMAND_Cyan          0x88
#define COMMAND_CyanBlue      0xA8

#define COMMAND_Blue          0x60
#define COMMAND_BlueViolet    0x50
#define COMMAND_Violet        0x70
#define COMMAND_VioletPink    0x48
#define COMMAND_Pink          0x68

#define ADDRESS_1    0x00
#define ADDRESS_2    0xF7

#define halfPeriodicTime  12    // 500/38;

class KobiIr {
public:
	enum State {
		unknown, off, on
	};

	KobiIr(byte pinIr, int brightness, KobiIr::State state,
			String color);
	void changeColor(String color);
	void animate(int speed);
	void turnOn();
	void turnOff();
	String getColor();
	void brighten();
	void darken();
	void setBrightness(int brightness);
	int getBrightness();
	State getState();

private:
	byte pinIr;
	byte pinLed;
	String color;
	int currentBrightness = 9;
	void irTurnOff();
	void irTurnOff(unsigned int time);
	void bitInitialization();
	void bitLow();
	void bitHigh();
	void sendByte(unsigned char data);
	void irTurnOn(unsigned int time);
	void sendCommand(unsigned char data);
	void resendCommand();
	State currentState = unknown;
};

#endif /* LIGHTMODULES_KOBIIR_H_ */
