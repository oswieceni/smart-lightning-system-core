#ifndef GESTURESENSORMODULES_SENSORAPDS9960_H_
#define GESTURESENSORMODULES_SENSORAPDS9960_H_

#include <Wire.h>
#include <ESP8266WiFi.h>
#include "../ThirdPartyLibraries/SparkFunAPDS9960/src/SparkFun_APDS9960.h"
#define APDS9960_INT    D9

class SensorAPDS9960 {
public:
	enum Gesture {
		UP, DOWN, LEFT, RIGHT, NEAR, FAR, NONE
	};

	SensorAPDS9960();
	Gesture handleGesture();
	void init();
private:
	SparkFun_APDS9960 apds9960 = SparkFun_APDS9960();
};

#endif /* GESTURESENSORMODULES_SENSORAPDS9960_H_ */
