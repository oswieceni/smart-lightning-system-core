#include "AcuModule.h"

#define DEBUG
using namespace std;

enum Mode {
	ManualControlled,
	MovementControlled,
	TimeControlled,
	WeatherControlled,
	IntensityControlled,
	FlowControlled,
	GestureControlled,
	NumberOfItems = GestureControlled
};

enum DeviceType {
	Bulb, Stripe
};

struct LightSource {
	int deviceId;
	IPAddress deviceAddress;
	DeviceType deviceType;
	Mode deviceMode = ManualControlled;
	int deviceTurn = 2;
	// Ustawienie od - do której godziny ma świecić źródło światła
	int StartHour = 8;
	int StartMinute = 00;
	int EndHour = 21;
	int EndMinute = 30;
	int requestDelay = 0;

	// Ustawienia do natężenia światła
	int targetIntensity = 300;
	int requestDelayAcu;

	// do trybu dyskoteki
	int flowSpeed;
	int sendFlow = 0;

	// do czujnika ruchu
	int moveLostDelay = 0;
	int searchMoveNow;
	int oldMove;
	int moveNull;
	int turnOffTimeMinute;
	int turnOffTimeSecond;
	int moveDetectionLost;
	int turnOffTimeHour;

	// do czujnika gestów
	int gesture;
	int indexColor = 0;

	// do manualnego
	int manualChange;
	int onOff;
	String colorManual;
	String intensityManual;

	// do profilu pogody
	bool weatherCheck = false;
	bool checkWeatherTime = false;

};

std::list<LightSource> lightSources;

std::list<LightSource>::iterator getLightSourceById(int deviceId) {
	std::list<LightSource>::iterator iterator;

	for (std::list<LightSource>::iterator it = lightSources.begin();
			it != lightSources.end(); it++) {
		if (it->deviceId == deviceId)
			iterator = it;
	}
	return iterator;
}


aREST rest = aREST();
Mode workMode = ManualControlled;

// Ustawienia do nateżenia światła
uint16_t intensity = 0;

// do czujnika ruchu
int detectedMove;

void sendSearchMove(int number);

// do profilu pogody
const char apiHost[] = "Host: 192.168.0.15:5000";
String colorWeatherTemp;


//do profilu gestów
String colorList[16] = { "ee1010", "f06e3a", "d27459", "f6a443", "ebd46a",
		"37c759", "7bf198", "83b08e", "61e1c8",
		"27a6d8", "044bb5", "5373ce", "813991", "e13b77", "e557ab", "ffffff" };


inline const char* ToString(Mode v) {
	switch (v) {
	case ManualControlled:
		return "Tryb manualny\n";
	case MovementControlled:
		return "Czujnik ruchu\n";
	case TimeControlled:
		return "Od czasu\n";
	case WeatherControlled:
		return "Od pogody\n";
	case IntensityControlled:
		return "Od natezenia swiatla\n";
	case FlowControlled:
		return "Przeplyw\n";
	case GestureControlled:
		return "Od gestow\n";
	default:
		return "[Unknown mode type]";
	}
}

void sendSearchGesture(bool activate) {
	WiFiClient GestureClient;
	if (!GestureClient.connect(Settings::getGestureModuleIP(), 80)) {
		Serial.println("Brak polaczenia gest.");
		return;
	}
	if (activate)
		GestureClient.println("GET /Gon?params=1 HTTP/1.1"); // GET http://192.168.0.131/Gon?params=0 lub params=1
	//else
	//GestureClient.println("GET /Gon?params=0 HTTP/1.1"); // GET http://192.168.0.131/Gon?params=0 lub params=1
	String host = "Host: " + Settings::getGestureModuleIP().toString() + ":80";
	GestureClient.println(host);
	GestureClient.println("Connection: close");
	GestureClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	GestureClient.stop();
}

void SetMode(int lightId, Mode mode) {
	getLightSourceById(lightId)->deviceMode = mode;

	sendSearchGesture(
			getLightSourceById(lightId)->deviceMode
					== Mode::GestureControlled);
	if (getLightSourceById(lightId)->deviceMode
			== Mode::MovementControlled) {
		getLightSourceById(lightId)->searchMoveNow = 1;
		sendSearchMove(1);
	}
	if (getLightSourceById(lightId)->deviceMode
			== Mode::WeatherControlled) {
		getLightSourceById(lightId)->weatherCheck = true;
	}
	Serial.printf("Zmieniono tryb zarowce: %d, na %s", lightId,
			ToString(getLightSourceById(lightId)->deviceMode));
}

int modeControl(String command) {
	int firstDash = command.indexOf("-");
	int lightId = command.substring(0, firstDash).toInt();
	int mode = command.substring(firstDash + 1).toInt();
	getLightSourceById(777776)->requestDelayAcu = 0;
	getLightSourceById(777780)->requestDelayAcu = 0;
	getLightSourceById(777779)->requestDelayAcu = 0;

#if defined(DEBUG)
	Serial.printf("Command: %s\n", command.c_str());
	Serial.printf("ID: %d Tryb: %d \n", lightId, mode);
#endif
	SetMode(lightId, (Mode) mode);
	return 1;
}

int manualControl(String command) {
#if defined(DEBUG)
	Serial.println(command);
#endif
	int firstDash = command.indexOf("-");
	int lightID = command.substring(0, firstDash).toInt();
	int secondDash = command.indexOf("-", firstDash + 1);
	getLightSourceById(lightID)->onOff = command.substring(firstDash + 1,
			secondDash).toInt();
	int nextDash = command.indexOf("-", secondDash + 1);
	getLightSourceById(lightID)->colorManual = command.substring(secondDash + 1,
			nextDash);
	getLightSourceById(lightID)->intensityManual = command.substring(
			nextDash + 1);
	Serial.printf("Z API:  Id zarowki: %d\n", lightID);
	getLightSourceById(lightID)->manualChange = 1;
	return 1;
}

int intensityControl(String command) {
	intensity = command.toInt();
	return 1;
}

int activityControl(String command) {
	int firstDash = command.indexOf("-");
	int lightID = command.substring(0, firstDash).toInt();
	int activityMode = command.substring(firstDash + 1).toInt();
	switch (activityMode) {
	case 0:
		Serial.println("Wybrano tryb komputer");
		getLightSourceById(lightID)->targetIntensity = 500;
		break;
	case 1:
		Serial.println("Wybrano tryb TV");
		getLightSourceById(lightID)->targetIntensity = 200;
		break;
	case 2:
		getLightSourceById(lightID)->targetIntensity = 450;
		Serial.println("Wybrano tryb ksiazka");
		break;
	}
	return 1;
}

void SetTime(int lightId, int startHour, int startMinute, int endHour,
		int endMinute) {
	getLightSourceById(lightId)->StartHour = startHour;
	getLightSourceById(lightId)->StartMinute = startMinute;
	getLightSourceById(lightId)->EndHour = endHour;
	getLightSourceById(lightId)->EndMinute = endMinute;
	Serial.printf(
			"Profil czasu zostal zaktualizowany. Aktualne godziny oswietlenia: %02d:%02d - %02d:%02d \n",
			startHour, startMinute, endHour, endMinute);
}

int timeControl(String command) {
#if defined(DEBUG)
	Serial.println(command);
#endif
	int firstDash = command.indexOf("-");
	int lightID = command.substring(0, firstDash).toInt();
	int secondDash = command.indexOf("-", firstDash + 1);
	String hourStart = command.substring(firstDash + 1, secondDash);
	int thirdDash = command.indexOf("-", secondDash + 1);
	String minuteStart = command.substring(secondDash + 1, thirdDash);
	int nextDash = command.indexOf("-", thirdDash + 1);
	String hourEnd = command.substring(thirdDash + 1, nextDash);
	String minuteEnd = command.substring(nextDash + 1);

	int startHour = hourStart.toInt();
	int startMinute = minuteStart.toInt();
	int endHour = hourEnd.toInt();
	int endMinute = minuteEnd.toInt();
	SetTime(lightID, startHour, startMinute, endHour, endMinute);
	return 1;
}

int gestureControl(String command) {
	Serial.printf("Gesture-com:\t%s\n", command.c_str());
	getLightSourceById(777776)->gesture = command.toInt();
	getLightSourceById(777780)->gesture = command.toInt();
	getLightSourceById(777779)->gesture = command.toInt();

	return 1;
}

int moveControl(String command) {
	detectedMove = command.toInt();
	getLightSourceById(777780)->searchMoveNow = 1;
	getLightSourceById(777776)->searchMoveNow = 1;
	getLightSourceById(777779)->searchMoveNow = 1;
#if defined(DEBUG)
	Serial.printf("Move ktore dostalem %d \n", detectedMove);
	Serial.printf("Ruch: %d", getLightSourceById(777780)->searchMoveNow);
#endif

	return detectedMove;
}

int flowControl(String command) {
	int firstDash = command.indexOf("-");
	int lightId = command.substring(0, firstDash).toInt();
	getLightSourceById(lightId)->flowSpeed =
			command.substring(firstDash + 1).toInt();
	getLightSourceById(lightId)->sendFlow = 1;
	Serial.printf("Szybkosc na zarowce: %d, %d\n", lightId,
			getLightSourceById(lightId)->flowSpeed);
	return getLightSourceById(lightId)->flowSpeed;
}

int moveTimeSettings(String command) {
	int firstDash = command.indexOf("-");
	int lightId = command.substring(0, firstDash).toInt();
	getLightSourceById(lightId)->moveLostDelay = command.substring(
			firstDash + 1).toInt();
	return getLightSourceById(lightId)->moveLostDelay;
}

AcuModule::AcuModule() {
	LightSource bulb1;
	bulb1.deviceId = 777780;
	bulb1.deviceAddress = Settings::getLightModuleIP(bulb1.deviceId);
	bulb1.deviceType = Bulb;
	lightSources.push_back(bulb1);
	LightSource led1;
	led1.deviceId = 777779;
	led1.deviceAddress = Settings::getLightModuleIP(led1.deviceId);
	led1.deviceType = Stripe;
	lightSources.push_back(led1);
	LightSource bulb2;
	bulb2.deviceId = 777776;
	bulb2.deviceAddress = Settings::getLightModuleIP(bulb2.deviceId);
	bulb2.deviceType = Bulb;
	lightSources.push_back(bulb2);
}

void AcuModule::printTime() {
	if (ntp.TimeIsChanged()) {
		currentTime = ntp.GetTime();
		Serial.printf("The time is %02d:%02d:%02d\n", currentTime.hour,
				currentTime.minute, currentTime.second);
	}
}

void AcuModule::SetManual(int lightId) {
	if (getLightSourceById(lightId)->onOff == 1) {
		sendTurnOnLight(lightId);
	} else {
		sendTurnOffLight(lightId);
	}
	sendColorLight(lightId, getLightSourceById(lightId)->colorManual);
	sendSetIntensityLight(lightId,
			getLightSourceById(lightId)->intensityManual);
	Serial.println("Dane otrzymane z mana: ");
	Serial.printf("ID: %d, ONOFF: %d, Kolor: %s, Natezenie: %s/n", lightId,
			getLightSourceById(lightId)->onOff,
			getLightSourceById(lightId)->colorManual.c_str(),
			getLightSourceById(lightId)->intensityManual.c_str());
}

void AcuModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(),
			Settings::getAcuIp(), Settings::getSubnet(),
			Settings::getGateway())) {
		ledDiodeRGBGesture.setColorRGB(200, 0, 0);
		return;
	}
	rest.function((char*) "mod", modeControl); // odbiór z API
	rest.function((char*) "acv", activityControl); // odbiór z API
	rest.function((char*) "inC", intensityControl); // odbiór czujnika natężenia
	rest.function((char*) "man", manualControl); // odbiór z API
	rest.function((char*) "Ges", gestureControl); // odbiór z czujnika gestów
	rest.function((char*) "mov", moveTimeSettings); // ustawienia dla profilu ruchu
	rest.function((char*) "moT", moveControl); // odbiór z czujnika ruchu
	rest.function((char*) "flo", flowControl); // odbiór z API
	rest.function((char*) "tim", timeControl); // odbiór z API
	WiFiClient client;
	Serial.println("Jestem ACU!");
	rest.set_id((char*) "777777");
	rest.set_name((char*) "Central");
	server.begin();
	Serial.println("Server started");
	ntp.Start(2390);
	ntp.GetTime();
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);
}

// ~*~*~*~*~*~*~ Do źródła światła
void AcuModule::sendTurnOnLight(int lightId) { // do włączenia źródła światła
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia pasek led.");
		return;
	}
	LightClient.println("GET /Lon?params=1 HTTP/1.1"); // GET http://192.168.0.13?/Lon
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	getLightSourceById(lightId)->deviceTurn = 1;
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	LightClient.stop();
}

void AcuModule::sendTurnOffLight(int lightId) { // do wyłączania źródła światła
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia pasek led.");
		return;
	}
	LightClient.println("GET /Lof?params=1 HTTP/1.1"); // GET http://192.168.0.13?/Lof
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	getLightSourceById(lightId)->deviceTurn = 0;
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	LightClient.stop();
}

void AcuModule::sendIntensityLight(int lightId, String step) { // rozjaśnienie/przyciemnienie źródła swiatła
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia zrodlo swiatla.");
		return;
	}
	LightClient.println("GET /Lin?params=" + step + " HTTP/1.1"); // GET http://192.168.0.133/Lof
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	LightClient.stop();
}

void AcuModule::sendColorLight(int lightId, String color) { // zmiana koloru ź©ódła światła
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia pasek led.");
		return;
	}
	LightClient.println("GET /LCo?params=" + color + " HTTP/1.1"); // GET http://192.168.0.133/Lof
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
	Serial.println(color);
#endif
	delay(10);
	LightClient.stop();
}

void AcuModule::sendSetIntensityLight(int lightId, String step) { // ustawienie jasności źródła światła na konkretny poziom
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia pasek led.");
		return;
	}
	LightClient.println("GET /LPI?params=" + step + " HTTP/1.1"); // GET http://192.168.0.133/Lof
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	LightClient.stop();
}

void AcuModule::sendSetFlowLight(int lightId, int flow) { // ustawienie przepływu
	WiFiClient LightClient;
	if (!LightClient.connect(Settings::getLightModuleIP(lightId), 80)) {
		Serial.println("Brak polaczenia z zarowka.");
		return;
	}
	LightClient.println("GET /Lfl?params=" + String(flow) + " HTTP/1.1"); // GET http://192.168.0.132/Flo
	String host = "Host: " + Settings::getLightModuleIP(lightId).toString()
			+ ":80";
	LightClient.println(host);
	LightClient.println("Connection: close");
	LightClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	LightClient.stop();
}

// ~*~*~*~*~*~  Do czujnia gestów
void AcuModule::sendSearchGesture() { // szukaj gestów
	WiFiClient GestureClient;
	if (!GestureClient.connect(Settings::getGestureModuleIP(), 80)) {
		Serial.println("Brak polaczenia z czujnikiem gestow.");
		return;
	}
	GestureClient.println("GET /Gon?params=1 HTTP/1.1"); // GET http://192.168.0.131/Lof
	String host = "Host: " + Settings::getGestureModuleIP().toString() + ":80";
	GestureClient.println(host);
	GestureClient.println("Connection: close");
	GestureClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	GestureClient.stop();
}

// ~*~*~*~*~* Do czujnika ruchu
void sendSearchMove(int number) { // szukaj ruch
	WiFiClient MoveClient;
	if (!MoveClient.connect(Settings::getMotionModuleIP(), 80)) {
		Serial.println("Brak polaczenia z czujnikiem ruchu.");
		return;
	}
	MoveClient.println("GET /moC?params=" + String(number) + " HTTP/1.1"); // GET http://192.168.0.135/moV
	String host = "Host: " + Settings::getMotionModuleIP().toString() + ":80";
	MoveClient.println(host);
	MoveClient.println("Connection: close");
	MoveClient.println();
#if defined(DEBUG)
	Serial.print(host);
#endif
	delay(10);
	MoveClient.stop();
}

// ~*~*~*~*~*~*~*~ Do trybów

void AcuModule::Everything() {
	// Do modułów
	WiFiClient intensityClient;
	// Do profilu pogdy
	WiFiClient apiClient;
	String colorWeather;
	int requestDelay = 0;
	for (list<LightSource>::iterator it = lightSources.begin();
			it != lightSources.end(); it++) {
		switch (it->deviceMode) {
		case ManualControlled:
			if (it->manualChange == 1) {
				SetManual(it->deviceId);
				it->manualChange = 0;
			}
			break;
	case TimeControlled:
			if (it->StartHour < it->EndHour) {
				if ((currentTime.hour > it->StartHour)
						&& (currentTime.hour < it->EndHour)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}

				} else if ((currentTime.hour == it->StartHour)
						&& (currentTime.minute >= it->StartMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
				} else if ((currentTime.hour == it->EndHour)
						&& (currentTime.minute <= it->EndMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
			} else
				if (it->deviceTurn != 0) {
					sendTurnOffLight(it->deviceId);
			}

			} else if ((it->StartHour == it->EndHour)
					&& (it->StartMinute <= it->EndMinute)) {
				if ((currentTime.hour == it->StartHour)
						&& (currentTime.minute >= it->StartMinute)
						&& (currentTime.minute <= it->EndMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
			} else
				if (it->deviceTurn != 0) {
					sendTurnOffLight(it->deviceId);
			}

			} else if ((it->StartHour == it->EndHour)
					&& (it->StartMinute > it->EndMinute)) {
				if ((currentTime.hour == it->StartHour)
						&& currentTime.minute > it->StartMinute) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
				} else if ((currentTime.hour > it->StartHour)
						|| (currentTime.hour < it->EndHour)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
				} else if ((currentTime.hour == it->EndHour)
						&& (currentTime.minute <= it->EndMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
			} else
				if (it->deviceTurn != 0) {
					sendTurnOffLight(it->deviceId);
			}
			} else if (it->StartHour > it->EndHour) {
				if ((currentTime.hour == it->StartHour)
						&& (currentTime.minute >= it->StartMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
				} else if ((currentTime.hour > it->StartHour)
						|| (currentTime.hour < it->EndHour)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
				} else if ((currentTime.hour == it->EndHour)
						&& (currentTime.minute <= it->EndMinute)) {
					if (it->deviceTurn != 1) {
						sendTurnOnLight(it->deviceId);
				}
			} else
				if (it->deviceTurn != 0) {
					sendTurnOffLight(it->deviceId);
			}

			} else if (it->deviceTurn != 0) {
				sendTurnOffLight(it->deviceId);
		}
		break;
	case WeatherControlled:
		// Api
			if (it->weatherCheck == true) {
			if (!apiClient.connect(Settings::getApiIP(), 5000)) {
				Serial.println("Brak polaczenia z api.");
				return;
			}
			apiClient.println("GET /weather/color HTTP/1.1");
			apiClient.println(apiHost);
			apiClient.println("Connection: close");
			apiClient.println();
			delay(10);
			while (apiClient.available()) {
				String line = apiClient.readStringUntil('\r');
				int addr_start = line.indexOf("color:");
				if (addr_start == -1) {
						Serial.print("Brak danych");
					delay(10);
				} else {
					colorWeather = line.substring(addr_start + 6);
						if (it->deviceTurn != 1) {
							sendTurnOnLight(it->deviceId);
					}
						//	Serial.println(colorWeatherTemp);
					if (colorWeather != colorWeatherTemp) {
						colorWeatherTemp = colorWeather;
#if defined(DEBUG)
						Serial.print("Kolor: ");
						Serial.println(colorWeather);
#endif
					}
						sendColorLight(it->deviceId, colorWeather);
					apiClient.stop();
						it->checkWeatherTime = true;
				}
			}
		}
			it->weatherCheck = false;
		if (currentTime.second
					> 0&& currentTime.second<30 && currentTime.minute == 30 && it->checkWeatherTime == true) {
				it->weatherCheck = true;
				it->checkWeatherTime = false;
		}
		break;

	case IntensityControlled:
			if (it->requestDelayAcu == 500000) {
				it->requestDelayAcu = 0;
		}

			if (it->requestDelayAcu == 0) {
			if (!intensityClient.connect(Settings::getIntensityModuleIp(),
					80)) {
				Serial.println("Brak polaczenia z czujnikiem natezenia.");
				return;
			}
			intensityClient.println("GET /inC?params=1 HTTP/1.1"); // GET http://192.168.0.136/inC
			String host = "Host: " + Settings::getIntensityModuleIp().toString()
					+ ":80";
			intensityClient.println(host);
			intensityClient.println("Connection: close");
			intensityClient.println();
#if defined(DEBUG)
			Serial.println("ACU wysyla polecenie do: ");
			Serial.println(host);
#endif
			delay(10);
			while (intensityClient.available()) {

				String line = intensityClient.readStringUntil('\r');
				int addr_start = line.indexOf("Intensity:");

				if (addr_start == -1) {
					delay(10);
				} else {

				}
				intensityClient.stop();
			}
			Serial.printf("Docelowe: %d Przed kalibracja: %d \n",
						it->targetIntensity, intensity);

				if (intensity > it->targetIntensity + 30) {
					sendIntensityLight(it->deviceId, "0");
				} else if (intensity < it->targetIntensity - 30) {
					sendIntensityLight(it->deviceId, "1");
			}
			Serial.println("Dobrano parametry");
		}
			it->requestDelayAcu++;

		break;

	case GestureControlled:
			if (it->gesture < 7)
				Serial.printf("Gesture:\t%d\n", (int) it->gesture);
			switch (it->gesture) {
		case 0:
				sendTurnOnLight(it->deviceId);
				it->gesture = 7;
			break;
		case 1:
				sendTurnOffLight(it->deviceId);
				it->gesture = 7;
			break;
		case 2:
				if (it->indexColor + 1 >= 16) {
					it->indexColor = 0;
				} else
					it->indexColor++;
				sendColorLight(it->deviceId, colorList[it->indexColor]);
				it->gesture = 7;
			break;
		case 3:
				if (it->indexColor - 1 < 0) {
					it->indexColor = 15;
				} else
					it->indexColor--;
				sendColorLight(it->deviceId, colorList[it->indexColor]);
				it->gesture = 7;
			break;
		case 4:
				sendSetFlowLight(it->deviceId, 3);
				it->gesture = 7;
			break;
		case 5:
				sendColorLight(it->deviceId, "ffffff");
				it->gesture = 7;
			break;
		case 6:
			Serial.print("Powtorz");
			break;
		default: // jesli gest zostal wykonany ustawiamy go poza zakresem dostepnych gestow
			break;
		}
		break;

	case MovementControlled:
			if (it->searchMoveNow == 1) {
				Serial.print("Znalazlem!");
				if (detectedMove != it->oldMove) {
					it->oldMove = detectedMove;
				if (detectedMove == 1) {
						sendTurnOnLight(it->deviceId);
						it->searchMoveNow = 0;
				} else {
						it->turnOffTimeSecond = currentTime.second;
						if ((currentTime.minute + it->moveLostDelay) > 59) {
							it->turnOffTimeMinute = currentTime.minute
									+ it->moveLostDelay
								- 60;
							it->turnOffTimeHour = currentTime.hour + 1;

						Serial.printf(
								"Mam wylaczyc swiatlo o %d:%d:%d \n",
									it->turnOffTimeHour, it->turnOffTimeMinute,
									it->turnOffTimeSecond);
							it->searchMoveNow = 0;
							it->moveNull = 1;

					} else {
							it->turnOffTimeHour = currentTime.hour;
							it->turnOffTimeMinute = currentTime.minute
									+ it->moveLostDelay;
						Serial.printf(
								"Mam wylaczyc swiatlo o %d:%d:%d \n",
									it->turnOffTimeHour, it->turnOffTimeMinute,
									it->turnOffTimeSecond);
							it->searchMoveNow = 0;
							it->moveNull = 1;
					}
					}
				}
			}
			if (detectedMove == 0 && it->moveNull == 1) {
				if ((currentTime.hour == it->turnOffTimeHour)
						&& (currentTime.minute >= (it->turnOffTimeMinute))
						&& (currentTime.second >= it->turnOffTimeSecond)) {
					sendTurnOffLight(it->deviceId);
					it->moveNull = 0;
			}
				if (currentTime.minute >= it->turnOffTimeMinute
						&& (currentTime.second >= it->turnOffTimeSecond)) {
					sendTurnOffLight(it->deviceId);
					it->moveNull = 0;
			}
		}

		break;
	case FlowControlled:
			if (it->sendFlow == 1) {
				sendTurnOnLight(it->deviceId);
				sendSetFlowLight(it->deviceId, it->flowSpeed);
				it->sendFlow = 0;
				Serial.printf("Tryb flow: ID:  %d\n", it->deviceId);
		}
	}
	}
}

void AcuModule::loop() {
	printTime();
	WiFiClient client = server.available();
	if (!client) {
		Everything();
	} else {
		while (!client.available()) {
			delay(1);
		}
		rest.handle(client);
	}
}
