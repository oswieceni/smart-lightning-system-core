#ifndef MODULES_INTENSITYMODULE_H_
#define MODULES_INTENSITYMODULE_H_

#include <WiFiUdp.h>
#include <WiFiClient.h>
#include "ESP8266WiFi.h"
#include "../WiFiModules/WiFiModule.h"
#include "../LightSensorModules/Bh1750Sensor.h"
#include "../ThirdPartyLibraries/aREST/aREST.h"
#include "../Settings.h"

class IntensityModule {
public:
	IntensityModule();
	void loop();
	void setup();
	uint16_t intensity = 0;
private:
	aREST rest = aREST();
	WiFiServer server = WiFiServer(80);
	void readIntensity();
	int requestDelayIntensity = 0;
	LedDiodeRGB ledDiodeRGBGesture = LedDiodeRGB(D3, D4, D8);
	Bh1750Sensor intensitySensor = Bh1750Sensor();

};

#endif /* MODULES_INTENSITYMODULE_H_ */
