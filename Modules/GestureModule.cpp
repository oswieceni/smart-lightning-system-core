#include "GestureModule.h"
#define DEBUG

bool search = false;


int gestureSearch(String command) {
	if (command.toInt() == 1) {
		search = true;
		Serial.println("Jedziemy!!!");
	}
	else
		search = false;
	return 1;
}

GestureModule::GestureModule() {
}

String toString(SensorAPDS9960::Gesture gesture) {
	switch(gesture) {
	case SensorAPDS9960::UP:
		return "UP";
		break;
	case SensorAPDS9960::DOWN:
		return "DOWN";
		break;
	case SensorAPDS9960::LEFT:
		return "LEFT";
		break;
	case SensorAPDS9960::RIGHT:
		return "RIGHT";
		break;
	case SensorAPDS9960::NEAR:
		return "NEAR";
		break;
	case SensorAPDS9960::FAR:
		return "FAR";
		break;
	case SensorAPDS9960::NONE:
		return "NONE";
		break;
	}
	return "OTHER?";
}

void GestureModule::readGesture() {
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);
	int gesture;
	if (requestDelayGesture == 10000) {
		requestDelayGesture = 0;
	}

	if (requestDelayGesture == 0) {
		ledDiodeRGBGesture.setColorRGB(LOW, LOW, LOW);
		gesture = sensorApds.handleGesture();
		if (gesture != SensorAPDS9960::NONE) {
			Serial.println(gesture);
			WiFiClient client;
			if (!client.connect(Settings::getAcuIp(), 80)) {
				Serial.println("Brak polaczenia.");
				return;
			}

			client.println("GET /Ges?params=" + String(gesture) + " HTTP/1.1"); // GET http://192.168.0.130/Ges
			String host = "Host: " + Settings::getAcuIp().toString() + ":80";
			client.println(host);
			client.println("Connection: close");
			client.println();
#if defined(DEBUG)
			Serial.print(host);
#endif
			client.stop();
			delay(100);
		}
	}
	requestDelayGesture++;
}

void GestureModule::setup() {
	ledDiodeRGBGesture.setColorRGB(250, 200, 0);
	WiFiModule wifi;
	if (!wifi.Connect(Settings::getSsid().c_str(),
			Settings::getPassword().c_str(), Settings::getGestureModuleIP(),
			Settings::getSubnet(), Settings::getGateway()))
	{
		ledDiodeRGBGesture.setColorRGB(200, 0, 0);
		return;
	}
	rest.function((char*) "Gon", gestureSearch);

	Serial.println("Jestem czujnikiem gestow!");

	WiFiClient client;
	rest.set_id((char*) "777781");
	rest.set_name((char*) "GestureModule");
	server.begin();
	Serial.println("Server started");
	sensorApds.init();
	ledDiodeRGBGesture.setColorRGB(0, 200, 0);
}

void GestureModule::loop() {
	WiFiClient client = server.available();
	if (!client) {
		if (search) {
			readGesture();
		}
	} else {
		while (!client.available()) {
			delay(1);
		}
		rest.handle(client);
	}
}
