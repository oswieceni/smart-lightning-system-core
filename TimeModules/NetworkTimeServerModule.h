/*
 * NetworkTimeServerModule.h
 *
 *  Created on: Apr 15, 2017
 *      Author: maciej
 */

#ifndef TIMEMODULES_NETWORKTIMESERVERMODULE_H_
#define TIMEMODULES_NETWORKTIMESERVERMODULE_H_

#include <WiFiUdp.h>
#include "../WiFiModules/WiFiModule.h"

#define NTP_PACKET_SIZE 48
#define UINT_MAX 		4294967295
#define GMT				2

class NetworkTimeServerModule {
public:
	struct MyTime {
		byte hour, minute, second;
	};

	NetworkTimeServerModule();
	void Start(unsigned udpPort);
	MyTime GetTime();
	bool TimeIsChanged();
private:
	unsigned sendNTPpacket(IPAddress& address);

	byte packetBuffer[NTP_PACKET_SIZE];

	const char* ntpServerName = "time.nist.gov";
	IPAddress timeServerIP; // time.nist.gov NTP serwer

	WiFiUDP udp;
	WiFiModule Wifi;

	uint32_t delay;
	bool wasReplay = false;

	MyTime currentTime;
	MyTime previousTime;
};

#endif /* TIMEMODULES_NETWORKTIMESERVERMODULE_H_ */
